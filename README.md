DrumGizmo BlackSmith
==========

Description
-----------
A DrumGizmo mapping for [Analogue Drums BlackSmith](https://www.bigfishaudio.com/BlackSmith) drumkit.

License
-------
This drum mapping is provided under the MIT license (see LICENSE).

[DrumGizmo](https://drumgizmo.org/) is free software licensed under the LPGL version 3 license.

[Analogue Drums BlackSmith](https://www.bigfishaudio.com/BlackSmith) samples are not part of 
this distribution! They are licensed under a commercial license!

Usage
-----
The "Samples" folder of the BlackSmith package needs to be linked or copied to
```
./instruments/samples
```
of this package. Afterwards just load the instrument and midimap into DrumGizmo.

BlackSmith contains three different snare samples. For simplicity three different
midi mappings are provided, to easily swap between the snare samples.

Channel Mapping
---------------
1. Kickdrum Left
2. Kickdrum Right
3. Kickdrum Sub
4. Snare Top
5. Snare Bottom
6. Tom 1
7. Tom 2
8. Tom 3
9. Tom 4
10. HiHat
11. Ride
12. Overhead Left
13. Overhead Right
14. Room Left
15. Room Right
16. Smash Mic

Midi Mapping 
------------
| Midi Number | Instrument           |
| ---         | ---                  |
| 29          | China                |
| 31          | China Stack          |
| 33          | Splash               |
| 35          | Kick Left            |
| 36          | Kick Right           |
| 37          | Snare Cross          |
| 38          | Snare                |
| 40          | Snare Cross          |
| 42          | HiHat Edge Closed    |
| 43          | Tom 4                |
| 44          | HiHat Pedaled        |
| 45          | Tom 3                |
| 46          | HiHat Edge Open      |
| 47          | Tom 2                |
| 48          | Tom 1                |
| 49          | Crash 1 Bell         |
| 51          | Ride Bow             |
| 52          | Crash 2 Edge         |
| 53          | Ride Bell            |
| 54          | Crash 1 Choked       |
| 55          | Crash 1 Edge         |
| 56          | Crash 2 Choked       |
| 57          | Crash 2 Bell         |
| 58          | FX Crash Bell        |
| 59          | FX Crash Edge        |
| 60          | HiHat Tip Tight      |
| 61          | HiHat Tip Closed     |
| 62          | HiHat Tip Loose      |
| 63          | HiHat Tip Semi-Open  |
| 64          | HiHat Tip Open       |
| 66          | HiHat Footsplash     |
| 68          | HiHat Edge Tight     |
| 69          | HiHat Edge Closed    |
| 70          | HiHat Edge Loose     |
| 71          | HiHat Edge Semi-Open |
| 72          | HiHat Edge Open      |
| 74          | Snare 1 Cross        |
| 75          | Snare 1              |
| 76          | Snare 2 Cross        |
| 77          | Snare 2              |
| 78          | Snare 3 Cross        |
| 79          | Snare 3              |

